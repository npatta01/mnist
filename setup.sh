
# OS Setup (ubunut 16.04)

sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt-get update
sudo apt-get install python3.6 python3-pip -y


echo "Install python dependencies"


pip3 install -r requirements.txt


echo "Installing Docker"

wget -qO- https://get.docker.com/ | sh
sudo usermod -a -G docker `whoami`
