
# Docker build
```
docker build -t mnist .

gcloud auth configure-docker
docker tag mnist us.gcr.io/np-training/mnist:1.0.0
docker push us.gcr.io/np-training/mnist:1.0.0

```

# Docker run
```
docker run -it --rm -p 5000:5000 -e PORT=5000  mnist 
```