FROM python:3.6.5

WORKDIR /app
ADD . . 

RUN pip install -r requirements.txt

ENV PORT 80
ENTRYPOINT ["python"]
CMD ["app.py"]